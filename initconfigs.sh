#!/bin/bash

if [[ "${BACKED_UP_HOME+x}" != "x" ]]; then
  BACKED_UP_HOME=$HOME/;
fi

select d in "apt"; do
  if [ -n "$d" ]; then
    # put your command here
    echo "chosen package manager: $d"
    MANAGER=$d
    break
  fi
done

mkdir -p $BACKED_UP_HOME/config

case $MANAGER in 
  "apt")
    if [ ! -d $BACKED_UP_HOME/config/bash ];then
      git clone https://gitlab.com/kalnarobert/bash-config.git $BACKED_UP_HOME/config/bash
      source $BACKED_UP_HOME/config/bash/create-symlink.sh
    else
      echo "bash is configured"
    fi
    if ! type "vim" > /dev/null 2>&1;then
      echo "installing vim";
      sudo apt install vim
    fi 
    if [ ! -d $BACKED_UP_HOME/config/vim ];then
      git clone https://gitlab.com/kalnarobert/vimconfig.git $BACKED_UP_HOME/config/vim
      source $BACKED_UP_HOME/config/vim/initialize-vim.sh
    else
      echo "vim is configured"
    fi 
    if ! type "tmux" > /dev/null 2>&1;then
      echo "installing tmux";
      sudo apt install tmux
    fi 
    if [ ! -d $BACKED_UP_HOME/config/tmux ];then
      git clone https://gitlab.com/kalnarobert/tmuxconfig.git $BACKED_UP_HOME/config/tmux
      source $BACKED_UP_HOME/config/tmux/initialize-tmux.sh
    else
      echo "tmux is configured"
    fi 
esac



